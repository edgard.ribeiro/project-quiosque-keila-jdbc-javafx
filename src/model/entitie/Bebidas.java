package model.entitie;

import java.io.Serializable;
import java.util.Date;

public class Bebidas implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nome;
	private Double venda;
	private Date dataVenda;
	private Double compra;
	private Date dataCompra;
	private Integer qtd;
	
	
	public Bebidas() {		
	}
	
	public Bebidas(Integer id, String nome, Double venda, Date dataVenda, Double compra, Date dataCompra, Integer qtd) {		
		this.id = id;
		this.nome = nome;
		this.venda = venda;
		this.dataVenda = dataVenda;
		this.compra = compra;
		this.dataCompra = dataCompra;
		this.qtd = qtd;		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getVenda() {
		return venda;
	}

	public void setVenda(Double venda) {
		this.venda = venda;
	}

	public Double getCompra() {
		return compra;
	}

	public void setCompra(Double compra) {
		this.compra = compra;
	}

	public Integer getQtd() {
		return qtd;
	}

	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}		

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bebidas other = (Bebidas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Bebidas [id=" + id + ", nome=" + nome + ", venda=" + venda + ", compra=" + compra
				+ ", qtd=" + qtd + "]";
	}	

}
