package model.services;

import java.text.ParseException;
import java.util.List;

import model.dao.BebidasDao;
import model.dao.DaoFactory;
import model.entitie.Bebidas;

public class BebidasService {
	
	private BebidasDao dao = DaoFactory.createBebidasDao();
	
	public List<Bebidas> findAll() {
		return dao.findAll();
	}
	
	public void saveOrUpdate(Bebidas obj) throws ParseException {
		if(obj.getId() == null) {
			dao.insert(obj);
		}else {
			dao.update(obj);
		}
	}
	
	public void remove(Bebidas obj) {
		dao.deleteById(obj.getId());
	}
}
