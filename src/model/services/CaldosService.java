package model.services;

import java.util.List;

import model.dao.CaldosDao;
import model.dao.DaoFactory;
import model.entitie.Caldos;

public class CaldosService {
	
	private CaldosDao dao = DaoFactory.createCaldosDao();
	
	public List<Caldos> findAll() {
		return dao.findAll();
	}
	
	public void saveOrUpdate(Caldos obj) {
		if(obj.getId() == null) {
			dao.insert(obj);
		}else {
			dao.update(obj);
		}
	}
	
	public void remove(Caldos obj) {
		dao.deleteById(obj.getId());
	}
}
