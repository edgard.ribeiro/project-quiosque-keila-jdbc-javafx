package model.dao;

import java.text.ParseException;
import java.util.List;

import model.entitie.Bebidas;

public interface BebidasDao {

	void insert(Bebidas obj) throws ParseException;
	void update(Bebidas obj);
	void deleteById(Integer id);
	Bebidas findById(Integer id);
	List<Bebidas> findAll();
}
