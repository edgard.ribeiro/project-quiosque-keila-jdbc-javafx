package model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import db.DB;
import db.DbException;
import db.DbIntegrityException;
import model.dao.BebidasDao;
import model.entitie.Bebidas;

public class BebidasDaoJDBC implements BebidasDao {

private Connection conn;
	
	public BebidasDaoJDBC(Connection conn) {
		this.conn = conn;
	}
	
	@Override
	public Bebidas findById(Integer id) {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(
				"SELECT * FROM Bebidas WHERE Id = ?");
			st.setInt(1, id);
			rs = st.executeQuery();
			if (rs.next()) {
				Bebidas obj = new Bebidas();
				obj.setId(rs.getInt("Id"));
				obj.setNome(rs.getString("Nome"));
				obj.setCompra(rs.getDouble("Compra"));
				obj.setDataCompra(rs.getDate("DataCompra"));
				obj.setVenda(rs.getDouble("Venda"));
				obj.setDataVenda(rs.getDate("DataVenda"));
				obj.setQtd(rs.getInt("Qtd"));
				return obj;
			}
			return null;
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}

	@Override
	public List<Bebidas> findAll() {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(
				"SELECT * FROM Bebidas ORDER BY Nome DESC");
			rs = st.executeQuery();

			List<Bebidas> list = new ArrayList<>();

			while (rs.next()) {				
				String dataCompra = rs.getString("DataCompra");
				String dataVenda = rs.getString("DataVenda");
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd"); 
				Date dataFormatadaCompra = formato.parse(dataCompra);	
				Date dataFormatadaVenda = formato.parse(dataVenda);
				Bebidas obj = new Bebidas();
				obj.setId(rs.getInt("Id"));
				obj.setNome(rs.getString("Nome"));
				obj.setCompra(rs.getDouble("Compra"));				
				obj.setDataCompra(new java.sql.Date(dataFormatadaCompra.getTime()));				
				obj.setVenda(rs.getDouble("Venda"));
				obj.setDataVenda(new java.sql.Date(dataFormatadaVenda.getTime()));				
				obj.setQtd(rs.getInt("Qtd"));
				list.add(obj);				
			}
			return list;
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		} catch (ParseException e) {			
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}

	@Override
	public void insert(Bebidas obj) {
		PreparedStatement st = null;		
		try {			
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	
	        
			st = conn.prepareStatement("INSERT INTO Bebidas (Nome, Compra, DataCompra, Venda, DataVenda, Qtd)"
					+ " VALUES (?, ?, ?, ?, ?, ?)", 
					Statement.RETURN_GENERATED_KEYS);

			st.setString(1, obj.getNome());
			st.setDouble(2, obj.getCompra());			
			st.setString(3, sdf.format(obj.getDataCompra()));			
			st.setDouble(4, obj.getVenda());
			st.setString(5, sdf.format(obj.getDataVenda()));				
			st.setInt(6, obj.getQtd());

			int rowsAffected = st.executeUpdate();
			
			if (rowsAffected > 0) {
				ResultSet rs = st.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					obj.setId(id);
				}
			}
			else {
				throw new DbException("Unexpected error! No rows affected!");
			}
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		} 
		finally {
			DB.closeStatement(st);
		}
	}

	@Override
	public void update(Bebidas obj) {
		PreparedStatement st = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			st = conn.prepareStatement("UPDATE BEBIDAS SET NOME = (?), COMPRA = (?), DATACOMPRA = (?), VENDA = (?), DATAVENDA = (?), QTD = (?) WHERE Id = ?");

			st.setString(1, obj.getNome());
			st.setDouble(2, obj.getCompra());			
			st.setString(3, sdf.format(obj.getDataCompra()));				
			st.setDouble(4, obj.getVenda());
			st.setString(5, sdf.format(obj.getDataVenda()));				
			st.setInt(6, obj.getQtd());
			st.setInt(7, obj.getId());

			st.executeUpdate();
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		} 
		finally {
			DB.closeStatement(st);
		}
	}

	@Override
	public void deleteById(Integer id) {
		PreparedStatement st = null;
		try {
			st = conn.prepareStatement(
				"DELETE FROM BEBIDAS WHERE Id = ?");

			st.setInt(1, id);

			st.executeUpdate();
		}
		catch (SQLException e) {
			throw new DbIntegrityException(e.getMessage());
		} 
		finally {
			DB.closeStatement(st);
		}
	}

}
