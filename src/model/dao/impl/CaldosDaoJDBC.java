package model.dao.impl;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import db.DB;
import db.DbException;
import db.DbIntegrityException;
import model.dao.CaldosDao;
import model.entitie.Caldos;

public class CaldosDaoJDBC implements CaldosDao {

private Connection conn;

	public CaldosDaoJDBC(Connection conn) {
		this.conn = conn;
	}
	
	@Override
	public Caldos findById(Integer id) {
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(
				"SELECT * FROM Caldos WHERE Id = ?");
			st.setInt(1, id);
			rs = st.executeQuery();
			if (rs.next()) {
				Caldos obj = new Caldos();
				obj.setId(rs.getInt("Id"));
				obj.setNome(rs.getString("Nome"));
				obj.setCompra(rs.getDouble("Compra"));
				obj.setDataCompra(rs.getDate("DataCompra"));
				obj.setVenda(rs.getDouble("Venda"));				
				obj.setQtd(rs.getInt("Qtd"));
				System.out.println(obj);
				return obj;
			}
			return null;
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}
	
	@Override
	public List<Caldos> findAll() {		
		PreparedStatement st = null;
		ResultSet rs = null;		
		
		try {
			st = conn.prepareStatement(
				"SELECT * FROM Caldos ORDER BY Nome");
			rs = st.executeQuery();

			List<Caldos> list = new ArrayList<>();

			while (rs.next()) {
				String data = rs.getString("DataCompra");
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd"); 
				Date dataFormatada = formato.parse(data);
				Caldos obj = new Caldos();
				obj.setId(rs.getInt("Id"));
				obj.setNome(rs.getString("Nome"));
				obj.setCompra(rs.getDouble("Compra"));				
				obj.setDataCompra(new java.sql.Date(dataFormatada.getTime()));
				obj.setVenda(rs.getDouble("Venda"));				
				obj.setQtd(rs.getInt("Qtd"));
				list.add(obj);				
			}
			return list;
		}
		catch (SQLException | ParseException e) {
			throw new DbException(e.getMessage());
		}
		finally {
			DB.closeStatement(st);
			DB.closeResultSet(rs);
		}
	}

	@Override
	public void insert(Caldos obj) {
		PreparedStatement st = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			st = conn.prepareStatement("INSERT INTO CALDOS (NOME, COMPRA, DATACOMPRA, VENDA, QTD)"
					+ " VALUES (?, ?, ?, ?, ?)", 
					Statement.RETURN_GENERATED_KEYS);

			st.setString(1, obj.getNome());
			st.setDouble(2, obj.getCompra());			
			st.setString(3, sdf.format(obj.getDataCompra()));			
			st.setDouble(4, obj.getVenda());			
			st.setInt(5, obj.getQtd());

			int rowsAffected = st.executeUpdate();
			
			if (rowsAffected > 0) {
				ResultSet rs = st.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					obj.setId(id);
				}
			}
			else {
				throw new DbException("Unexpected error! No rows affected!");
			}
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		} 
		finally {
			DB.closeStatement(st);
		}
	}

	@Override
	public void update(Caldos obj) {
		PreparedStatement st = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			st = conn.prepareStatement("UPDATE CALDOS SET NOME = (?), COMPRA = (?), DATACOMPRA = (?), VENDA = (?), QTD = (?) WHERE Id = ?");

			st.setString(1, obj.getNome());
			st.setDouble(2, obj.getCompra());			
			st.setString(3, sdf.format(obj.getDataCompra()));				
			st.setDouble(4, obj.getVenda());			
			st.setInt(5, obj.getQtd());
			st.setInt(6, obj.getId());

			st.executeUpdate();
		}
		catch (SQLException e) {
			throw new DbException(e.getMessage());
		} 
		finally {
			DB.closeStatement(st);
		}
	}

	@Override
	public void deleteById(Integer id) {
		PreparedStatement st = null;
		try {
			st = conn.prepareStatement(
				"DELETE FROM Caldos WHERE Id = ?");

			st.setInt(1, id);

			st.executeUpdate();
		}
		catch (SQLException e) {
			throw new DbIntegrityException(e.getMessage());
		} 
		finally {
			DB.closeStatement(st);
		}
	}

}
