package model.dao;

import java.util.List;

import model.entitie.Caldos;

public interface CaldosDao {

	void insert(Caldos obj);
	void update(Caldos obj);
	void deleteById(Integer id);
	Caldos findById(Integer id);
	List<Caldos> findAll();
}
