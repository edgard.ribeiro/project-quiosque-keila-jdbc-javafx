package model.dao;

import db.DB;
import model.dao.impl.BebidasDaoJDBC;
import model.dao.impl.CaldosDaoJDBC;

public class DaoFactory {

	public static CaldosDao createCaldosDao() {
		return new CaldosDaoJDBC(DB.getConnection());
	}
	
	public static BebidasDao createBebidasDao() {
		return new BebidasDaoJDBC(DB.getConnection());
	}
}
