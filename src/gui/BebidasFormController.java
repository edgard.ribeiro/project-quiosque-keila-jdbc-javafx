package gui;

import java.net.URL;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import db.DbException;
import gui.listeners.DataChangeListener;
import gui.utils.Alerts;
import gui.utils.Constraints;
import gui.utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.entitie.Bebidas;
import model.exceptions.ValidationException;
import model.services.BebidasService;

public class BebidasFormController implements Initializable{
	
	@FXML
	private TextField txtId;
	
	@FXML
	private TextField txtNome;
	
	@FXML
	private TextField txtCompra;
	
	@FXML
	private DatePicker txtDataCompra;	
	
	@FXML
	private TextField txtVenda;
	
	@FXML
	private DatePicker txtDataVenda;
	
	@FXML
	private TextField txtQtd;
	
	@FXML
	private Label labelErrorNome;
	
	@FXML
	private Label labelErrorCompra;
	
	@FXML
	private Label labelErrorVenda;
	
	@FXML
	private Label labelErrorDataCompra;
	
	@FXML
	private Label labelErrorDataVenda;
	
	@FXML
	private Label labelErrorQtd;
	
	@FXML
	private Button btnSalvar;
	
	@FXML
	private Button btnCancelar;
	
	private Bebidas entity;
	
	private BebidasService bebidasService;
	
	private List<DataChangeListener> dataChangeListeners = new ArrayList<>();
	
	public void setBebidas(Bebidas entity) {
		this.entity = entity;
	}
	
	public void setBebidasService(BebidasService bebidasService) {
		this.bebidasService = bebidasService;
	}
	
	public void subscribeDataChangeListener(DataChangeListener listener) {
		dataChangeListeners.add(listener);
	}
	
	@FXML
	public void onBtnSalvarAction(ActionEvent event) throws ParseException {
		if(entity == null) {
			throw new IllegalStateException("Entidade estava nulo");
		}
		
		if(bebidasService == null) {
			throw new IllegalStateException("Serviço estava nulo");
		}
		
		try {
			entity = getFormData();
			bebidasService.saveOrUpdate(entity);
			notifyDataChangeListener();
			Utils.currentStage(event).close();
		}catch(DbException e) {
			e.printStackTrace();
			Alerts.showAlert("Erro ao salvar objeto", null, e.getMessage(), AlertType.ERROR);
		}catch(ValidationException e) {
			setErrorMensages(e.getErrors());
		}
	}	

	private void notifyDataChangeListener() {
		for(DataChangeListener listener : dataChangeListeners) {
			listener.onDataChange();
		}		
	}

	@FXML
	public void onBtnCancelarAction(ActionEvent event) {
		Utils.currentStage(event).close();
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {	
		initializaNode();
	}
	
	private void initializaNode() {
		Constraints.setTextFieldInteger(txtId);
		Constraints.setTextFieldInteger(txtQtd);
		Constraints.setTextFieldDouble(txtCompra);
		Utils.formatDatePicker(txtDataCompra, "dd/MM/yyyy");
		Constraints.setTextFieldDouble(txtVenda);
		Utils.formatDatePicker(txtDataVenda, "dd/MM/yyyy");
		Constraints.setTextFieldMaxLength(txtNome, 60);		
	}
	
	public void updateFormData() {
		if(entity == null) {
			throw new IllegalStateException("Entidade era nula");
		}
		txtId.setText(String.valueOf(entity.getId()));			
		txtNome.setText(entity.getNome());			
		Locale.setDefault(Locale.US);
		txtCompra.setText(String.format("%.2f", entity.getCompra()));			
		if(entity.getDataCompra() != null) {				
			int ano = Integer.parseInt(entity.getDataCompra().toString().substring(0,4));
			int mes = Integer.parseInt(entity.getDataCompra().toString().substring(5,7));
			int dia = Integer.parseInt(entity.getDataCompra().toString().substring(8,10));
			txtDataCompra.setValue(LocalDate.of(ano, mes, dia));					
		}		
		Locale.setDefault(Locale.US);
		txtVenda.setText(String.format("%.2f", entity.getVenda()));
		if(entity.getDataVenda() != null) {
			int ano = Integer.parseInt(entity.getDataVenda().toString().substring(0,4));
			int mes = Integer.parseInt(entity.getDataVenda().toString().substring(5,7));
			int dia = Integer.parseInt(entity.getDataVenda().toString().substring(8,10));
			txtDataVenda.setValue(LocalDate.of(ano, mes, dia));
		}
		txtQtd.setText(String.valueOf(entity.getQtd()));				
	}
	
	private Bebidas getFormData() {
		Bebidas obj = new Bebidas();
		ValidationException exception = new ValidationException("Validation Error");
		
		obj.setId(Utils.tryParseToInt(txtId.getText()));
		
		if(txtNome.getText() == null || txtNome.getText().trim().equals("")) {
			exception.addErrors("Nome", "O campo não pode ser vazio");
		}
		obj.setNome(txtNome.getText());	
		
		if(txtCompra.getText() == null || txtCompra.getText().trim().equals("")) {
			exception.addErrors("Compra", "O campo não pode ser vazio");
		}		
		obj.setCompra(Utils.tryParseToDouble(txtCompra.getText()));		
		
		if(txtDataCompra.getValue() == null) {
			exception.addErrors("Data Compra", "O campo não pode ser vazio");
		}else {
			Instant instantCompra = Instant.from(txtDataCompra.getValue().atStartOfDay(ZoneId.systemDefault()));
			obj.setDataCompra(Date.from(instantCompra));
		}		
		
		if(txtVenda.getText() == null || txtVenda.getText().trim().equals("")) {
			exception.addErrors("Venda", "O campo não pode ser vazio");		
		}
		obj.setVenda(Utils.tryParseToDouble(txtVenda.getText()));
		
		if(txtDataVenda.getValue() == null) {
			exception.addErrors("Data Venda", "O campo não pode ser vazio");
		}else {
			Instant instantVenda = Instant.from(txtDataVenda.getValue().atStartOfDay(ZoneId.systemDefault()));
			obj.setDataVenda(Date.from(instantVenda));
		}		
		
		if(txtQtd.getText() == null || txtQtd.getText().trim().equals("")) {
			exception.addErrors("Qtd", "O campo não pode ser vazio");
		}
		obj.setQtd(Utils.tryParseToInt(txtQtd.getText()));
		
		if(exception.getErrors().size() > 0) {
			throw exception;
		}
		return obj;
	}	
	
	private void setErrorMensages(Map<String, String> errors) {
		Set<String> fields = errors.keySet();
		
		labelErrorNome.setText((fields.contains("Nome") ? errors.get("Nome") : ""));
		
		labelErrorCompra.setText((fields.contains("Compra") ? errors.get("Compra") : ""));
		
		labelErrorDataCompra.setText((fields.contains("Data Compra") ? errors.get("Data Compra") : ""));
		
		labelErrorVenda.setText((fields.contains("Venda") ? errors.get("Venda") : ""));
		
		labelErrorDataVenda.setText((fields.contains("Data Venda") ? errors.get("Data Venda") : ""));
		
		labelErrorQtd.setText((fields.contains("Qtd") ? errors.get("Qtd") : ""));		
	}	
	
	public static String removeCaracters(String entrada){
	     Pattern numericos = Pattern.compile("[0-99]",Pattern.CASE_INSENSITIVE);
	     Matcher encaixe = numericos.matcher(entrada);
	     StringBuffer saida = new StringBuffer();
	     while(encaixe.find())
	        saida.append(encaixe.group());
	     return saida.toString();
}

}
