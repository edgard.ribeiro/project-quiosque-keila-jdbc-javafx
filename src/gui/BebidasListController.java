package gui;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import application.Main;
import db.DbIntegrityException;
import gui.listeners.DataChangeListener;
import gui.utils.Alerts;
import gui.utils.Utils;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.entitie.Bebidas;
import model.services.BebidasService;

public class BebidasListController implements Initializable, DataChangeListener {

	@FXML
	private TableView<Bebidas> tableViewBebidas;

	@FXML
	private TableColumn<Bebidas, Integer> tableColumnId;

	@FXML
	private TableColumn<Bebidas, String> tableColumnNome;

	@FXML
	private TableColumn<Bebidas, Bebidas> tableColumnEDIT;

	@FXML
	private TableColumn<Bebidas, Bebidas> tableColumnREMOVE;

	@FXML
	private TableColumn<Bebidas, Double> tableColumnPrecoVenda;
	
	@FXML
	private TableColumn<Bebidas, Double> tableColumnPrecoCompra;
	
	@FXML
	private TableColumn<Bebidas, Date> tableColumnDataVenda;
	
	@FXML
	private TableColumn<Bebidas, Date> tableColumnDataCompra;
	
	@FXML
	private TableColumn<Bebidas, Integer> tableColumnQtd;	

	@FXML
	private Button btnNovo;

	private BebidasService bebidasService;

	private ObservableList<Bebidas> obsList;

	@FXML
	public void onBtNovoAction(ActionEvent event) {
		Stage parentStage = Utils.currentStage(event);
		Bebidas obj = new Bebidas();
		createDialogForm(obj, "/gui/BebidasForm.fxml", parentStage);
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		initializeNode();
	}

	public void setBebidasService(BebidasService bebidasService) {
		this.bebidasService = bebidasService;
	}

	private void initializeNode() {
		tableColumnId.setCellValueFactory(new PropertyValueFactory<>("Id"));
		tableColumnNome.setCellValueFactory(new PropertyValueFactory<>("Nome"));
		tableColumnPrecoVenda.setCellValueFactory(new PropertyValueFactory<>("Venda"));		
		Utils.formatTableColumnDouble(tableColumnPrecoVenda, 2);
		tableColumnDataVenda.setCellValueFactory(new PropertyValueFactory<>("DataVenda"));
		Utils.formatTableColumnDate(tableColumnDataVenda, "dd/MM/yyyy");
		tableColumnPrecoCompra.setCellValueFactory(new PropertyValueFactory<>("Compra"));
		Utils.formatTableColumnDouble(tableColumnPrecoCompra, 2);
		tableColumnDataCompra.setCellValueFactory(new PropertyValueFactory<>("DataCompra"));
		Utils.formatTableColumnDate(tableColumnDataCompra, "dd/MM/yyyy");
		tableColumnQtd.setCellValueFactory(new PropertyValueFactory<>("Qtd"));

		Stage stage = (Stage) Main.getMainScene().getWindow();
		tableViewBebidas.prefHeightProperty().bind(stage.heightProperty());
	}

	public void updateTableView() {
		if (bebidasService == null) {
			throw new IllegalStateException("Serviço estava nulo");
		}
		List<Bebidas> list = bebidasService.findAll();
		obsList = FXCollections.observableArrayList(list);
		tableViewBebidas.setItems(obsList);
		initEditButtons();
		initRemoveButtons();
	}

	private void createDialogForm(Bebidas obj, String absoluteName, Stage parentStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(absoluteName));
			Pane pane = loader.load();

			BebidasFormController controller = loader.getController();
			controller.setBebidas(obj);
			controller.setBebidasService(new BebidasService());
			controller.subscribeDataChangeListener(this);
			controller.updateFormData();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Insira ou atualiza nova bebida");
			dialogStage.setScene(new Scene(pane));
			dialogStage.setResizable(false);
			dialogStage.initOwner(parentStage);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
			Alerts.showAlert("IO Exception", "Error loading view", e.getMessage(), AlertType.ERROR);
		}
	}

	@Override
	public void onDataChange() {
		updateTableView();
	}

	private void initEditButtons() {
		tableColumnEDIT.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		tableColumnEDIT.setCellFactory(param -> new TableCell<Bebidas, Bebidas>() {
			private final Button button = new Button("Editar");

			@Override
			protected void updateItem(Bebidas obj, boolean empty) {
				super.updateItem(obj, empty);
				if (obj == null) {
					setGraphic(null);
					return;
				}
				setGraphic(button);
				button.setOnAction(event -> createDialogForm(obj, "/gui/BebidasForm.fxml", Utils.currentStage(event)));
				button.setStyle("-fx-base: #0459f7;");
				button.setTextFill(Paint.valueOf("#ffffff"));
			}
		});
	}

	private void initRemoveButtons() {
		tableColumnREMOVE.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		tableColumnREMOVE.setCellFactory(param -> new TableCell<Bebidas, Bebidas>() {
			private final Button button = new Button("Deletar");

			@Override
			protected void updateItem(Bebidas obj, boolean empty) {
				super.updateItem(obj, empty);
				if (obj == null) {
					setGraphic(null);
					return;
				}
				setGraphic(button);
				button.setOnAction(event -> removeEntity(obj));
				button.setStyle("-fx-base: #f10101;");	
				button.setTextFill(Paint.valueOf("#ffffff"));
			}
		});
	}

	private void removeEntity(Bebidas obj) {		
		Optional<ButtonType> result = Alerts.showConfirmation("Confirmação", "Tem certeza que quer deletar?");
		
		if(result.get() == ButtonType.OK) {
			if(bebidasService == null) {
				throw new IllegalStateException("Serviço estava nulo");
			}
			
			try {
				bebidasService.remove(obj);
				updateTableView();
			}catch(DbIntegrityException e) {
				Alerts.showAlert("Error removing object", null, e.getMessage(), AlertType.ERROR);
			}
		}
	}

}
