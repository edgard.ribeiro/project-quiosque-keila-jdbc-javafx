package gui;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import application.Main;
import db.DbIntegrityException;
import gui.listeners.DataChangeListener;
import gui.utils.Alerts;
import gui.utils.Utils;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.entitie.Caldos;
import model.services.CaldosService;

public class CaldosListController implements Initializable, DataChangeListener {

	@FXML
	private TableView<Caldos> tableViewCaldos;

	@FXML
	private TableColumn<Caldos, Integer> tableColumnId;

	@FXML
	private TableColumn<Caldos, String> tableColumnNome;

	@FXML
	private TableColumn<Caldos, Caldos> tableColumnEDIT;

	@FXML
	private TableColumn<Caldos, Caldos> tableColumnREMOVE;

	@FXML
	private TableColumn<Caldos, Double> tableColumnPrecoVenda;
	
	@FXML
	private TableColumn<Caldos, Double> tableColumnPrecoCompra;
	
	@FXML
	private TableColumn<Caldos, Date> tableColumnDataCompra;
	
	@FXML
	private TableColumn<Caldos, Integer> tableColumnQtd;
	
	
	@FXML
	private Button btnNovo;

	private CaldosService caldosService;

	private ObservableList<Caldos> obsList;

	@FXML
	public void onBtNovoAction(ActionEvent event) {
		Stage parentStage = Utils.currentStage(event);
		Caldos obj = new Caldos();
		createDialogForm(obj, "/gui/CaldosForm.fxml", parentStage);
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		initializeNode();
	}

	public void setCaldosService(CaldosService caldosService) {
		this.caldosService = caldosService;
	}

	private void initializeNode() {
		tableColumnId.setCellValueFactory(new PropertyValueFactory<>("Id"));
		tableColumnNome.setCellValueFactory(new PropertyValueFactory<>("Nome"));
		tableColumnPrecoVenda.setCellValueFactory(new PropertyValueFactory<>("Venda"));
		Utils.formatTableColumnDouble(tableColumnPrecoVenda, 2);
		tableColumnPrecoCompra.setCellValueFactory(new PropertyValueFactory<>("Compra"));
		Utils.formatTableColumnDouble(tableColumnPrecoCompra, 2);
		tableColumnDataCompra.setCellValueFactory(new PropertyValueFactory<>("DataCompra"));
		Utils.formatTableColumnDate(tableColumnDataCompra, "dd/MM/yyyy");
		tableColumnQtd.setCellValueFactory(new PropertyValueFactory<>("Qtd"));

		Stage stage = (Stage) Main.getMainScene().getWindow();
		tableViewCaldos.prefHeightProperty().bind(stage.heightProperty());
	}

	public void updateTableView() {
		if (caldosService == null) {
			throw new IllegalStateException("Serviço estava nulo");
		}
		List<Caldos> list = caldosService.findAll();
		obsList = FXCollections.observableArrayList(list);
		tableViewCaldos.setItems(obsList);
		initEditButtons();
		initRemoveButtons();
	}

	private void createDialogForm(Caldos obj, String absoluteName, Stage parentStage) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(absoluteName));
			Pane pane = loader.load();

			CaldosFormController controller = loader.getController();
			controller.setCaldos(obj);
			controller.setCaldosService(new CaldosService());
			controller.subscribeDataChangeListener(this);
			controller.updateFormData();

			Stage dialogStage = new Stage();
			dialogStage.setTitle("Insira novo caldo");
			dialogStage.setScene(new Scene(pane));
			dialogStage.setResizable(false);
			dialogStage.initOwner(parentStage);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.showAndWait();
		} catch (IOException e) {
			Alerts.showAlert("IO Exception", "Error loading view", e.getMessage(), AlertType.ERROR);
		}
	}

	@Override
	public void onDataChange() {
		updateTableView();
	}

	private void initEditButtons() {
		tableColumnEDIT.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		tableColumnEDIT.setCellFactory(param -> new TableCell<Caldos, Caldos>() {
			private final Button button = new Button("Editar");

			@Override
			protected void updateItem(Caldos obj, boolean empty) {
				super.updateItem(obj, empty);
				if (obj == null) {
					setGraphic(null);
					return;
				}
				setGraphic(button);
				button.setOnAction(event -> createDialogForm(obj, "/gui/CaldosForm.fxml", Utils.currentStage(event)));
				button.setStyle("-fx-base: #0459f7;");
				button.setTextFill(Paint.valueOf("#ffffff"));
			}
		});
	}

	private void initRemoveButtons() {
		tableColumnREMOVE.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
		tableColumnREMOVE.setCellFactory(param -> new TableCell<Caldos, Caldos>() {
			private final Button button = new Button("Deletar");

			@Override
			protected void updateItem(Caldos obj, boolean empty) {
				super.updateItem(obj, empty);
				if (obj == null) {
					setGraphic(null);
					return;
				}
				setGraphic(button);
				button.setOnAction(event -> removeEntity(obj));
				button.setStyle("-fx-base: #f10101;");	
				button.setTextFill(Paint.valueOf("#ffffff"));
			}
		});
	}

	private void removeEntity(Caldos obj) {		
		Optional<ButtonType> result = Alerts.showConfirmation("Confirmação", "Tem certeza que quer deletar?");
		
		if(result.get() == ButtonType.OK) {
			if(caldosService == null) {
				throw new IllegalStateException("Serviço estava nulo");
			}
			
			try {
				caldosService.remove(obj);
				updateTableView();
			}catch(DbIntegrityException e) {
				Alerts.showAlert("Error removing object", null, e.getMessage(), AlertType.ERROR);
			}
		}
	}

}
