package application;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TesteDateMain {

	public static void main(String[] args) throws ParseException {
		DateFormat sdf = new SimpleDateFormat("yyyy–MM–dd", Locale.ENGLISH);// Make sure to use Locale
        String dateTimeString = "2020–03–01 03:15 PM";// The string copied from the exception you have got
        Date date = sdf.parse(dateTimeString);
        System.out.println(sdf.format(date));
		
//		SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		isoFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
//
//		java.util.Date date = isoFormat.parse("2010-05-23T22:01:02");
//		TimeZone.setDefault(TimeZone.getTimeZone("America/Los_Angeles"));
//		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
//		System.out.println(sqlDate);
//		// This will print 2010-05-23
		
//		String text = "2024-04-30";
//		String format = "yyyy-MM-dd";
//		// usando Locale.US (Inglês Americano)
//		SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
//		System.out.println(dateFormat.parse(text));
	}

}
